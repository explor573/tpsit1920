import 'package:covid/favourite.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'api.dart';
import 'globals.dart';

class PageCountry extends StatefulWidget {
  PageCountry({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _PageCountryState createState() => _PageCountryState();
}

class _PageCountryState extends State<PageCountry> {
  bool saved;
  Country current;
  Image flag;
  @override
  void initState() {
    saved = savedCountry.contains(widget.title);
    //assign();
    _getData();
    super.initState();
  }

  void _getData() async {
    current = await fetchCountry(widget.title);
    flag = Image.network(current.flag);
    setState(() {});
  }

  Future<Widget> _saved() async {
    return (saved)
        ? IconButton(
            icon: Icon(Icons.star),
            onPressed: () async {
              dao.deleteCountry(
                  savedCountryRaw.firstWhere((x) => x.country == widget.title));
              savedCountryRaw = await dao.findAllCountries();
              assign();
              saved = false;
              setState(() {});
            },
          )
        : IconButton(
            icon: Icon(Icons.star_border),
            onPressed: () async {
              dao.insertCountry(SavedCountry(null, widget.title));
              savedCountryRaw = await dao.findAllCountries();
              assign();
              saved = true;
              setState(() {});
            },
          );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
            title: TextField(
          controller: textController,
          onChanged: (x) {
            Navigator.of(context).pop();
          },
          textAlignVertical: TextAlignVertical.bottom,
          decoration: InputDecoration(
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              hintStyle: TextStyle(color: Colors.grey),
              hasFloatingPlaceholder: false,
              hintText: 'Search',
              suffixIcon: Icon(Icons.search, color: Colors.grey)),
          style: TextStyle(color: Colors.white),
        )),
        body: (current != null)
            ? Center(
                child: Column(children: [
                Card(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Container(
                          child: flag,
                          height: 30,
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.black, width: 1))),
                    ),
                    Text(
                      widget.title,
                      style: TextStyle(fontSize: 20),
                    ),
                    Container(
                      //alignment: Alignment.centerRight,
                      child: FutureBuilder(
                          future: _saved(),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              return snapshot.data;
                            } else
                              return Container();
                          }),
                    ),
                  ],
                )),
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '${current.deaths ?? '0'}',
                        ),
                        subtitle: Text(
                          'deaths',
                          style: TextStyle(color: Colors.red),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          '${current.critical ?? '0'}',
                        ),
                        subtitle: Text(
                          'criticals',
                          style: TextStyle(color: Colors.orange),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          '${current.recovered ?? '0'}',
                        ),
                        subtitle: Text(
                          'recovered',
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          '${current.tests ?? '0'}',
                        ),
                        subtitle: Text('tests'),
                      ),
                      ListTile(
                        title: Text(
                          '${current.active ?? '0'}',
                        ),
                        subtitle: Text('actives'),
                      )
                    ],
                  ),
                )
              ]))
            : Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                    Container(
                        height: 100,
                        width: 100,
                        child: CircularProgressIndicator())
                  ])));
  }
}
