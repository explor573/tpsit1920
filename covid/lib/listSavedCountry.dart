import 'package:covid/favourite.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'api.dart';
import 'globals.dart';

class ListSavedCountry extends StatefulWidget {
  BuildContext ctx;
  ListSavedCountry({this.ctx});
  @override
  _ListSavedCountryState createState() => _ListSavedCountryState();
}

class _ListSavedCountryState extends State<ListSavedCountry> {
  void initState() {
    assign();
    super.initState();
  }

  Future<Widget> _flag(String country) async {
    Country temp = await fetchCountry(country);
    return Container(
        height: 30,
        width: 100,
        
            child: Image.network(temp.flag));
              
  }

  Future<Widget> _saved(String country) async {
    return IconButton(
      icon: Icon(Icons.delete),
      onPressed: () async {
        dao.deleteCountry(
            savedCountryRaw.firstWhere((x) => x.country == country));
        savedCountryRaw = await dao.findAllCountries();
        assign();
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: savedCountry.length,
        itemBuilder: (context, index) => ListTile(
              trailing: FutureBuilder(
                  future: _saved(savedCountry.toList()[index]),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data;
                    } else
                      return Container();
                  }),
              leading: FutureBuilder(
                  future: _flag(savedCountry.toList()[index]),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data;
                    } else
                      return Container();
                  }),
              onTap: () {
                selectedCountry = savedCountry.toList()[index];
                Navigator.of(widget.ctx).pushNamed('/country');
              },
              title: Text(savedCountry.toList()[index]),
            ));
  }
}
