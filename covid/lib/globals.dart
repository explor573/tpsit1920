import 'dart:convert';

import 'package:covid/favourite.dart';
import 'package:flutter/material.dart';
import 'api.dart' as api;

List<List<String>> list;
List<List<String>> filtredList;
String selectedCountry;
List<SavedCountry> savedCountryRaw;
Set<String> savedCountry={};
TextEditingController textController = TextEditingController();
ThemeData tema;
var dao;

  void assign() {
    savedCountry={};
    savedCountryRaw.forEach((x) {
      
      savedCountry.add(x.country);
    });
  }

   void assigment() async {
    List<dynamic> temp;

    final response = await api.getAll();

    if (response.statusCode == 200) {
      temp = (json.decode(response.body));
    } else {
      throw Exception('Failed to download the countries');
    }
    list = [];
    temp.forEach((element) {
      list.add(<String>[
        element['country']??'',
        element['countryInfo']['iso2']??'',
        element['countryInfo']['iso3']??''
      ]);
      
    });
    list.forEach((x){
      print(x);
    });
    filtredList=list.where((x) {
                    bool b = (x[0] ?? '')
                            .toUpperCase()
                            .startsWith(textController.text.toUpperCase()) ||
                        (x[1] ?? '')
                            .toUpperCase()
                            .startsWith(textController.text.toUpperCase()) ||
                        (x[2] ?? '')
                            .toUpperCase()
                            .startsWith(textController.text.toUpperCase());
                    return b;
                  }).toList();
   
  }
