import 'package:covid/home.dart';
import 'package:covid/list.dart';
import 'package:covid/listSavedCountry.dart';
import 'package:covid/pageCountry.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';
import 'database.dart';
import 'list.dart';
import 'globals.dart' as g;
import 'dart:io';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final database =
      await $FloorAppDatabase.databaseBuilder('flutter_database.db').build();
  g.dao = database.savedCountryDao;
  g.savedCountryRaw = await g.dao.findAllCountries();
  g.assigment();
  print(g.list);
  g.assign();
  g.tema = ThemeData(
    primaryColor: Colors.grey[800],
  );
  runApp(MaterialApp(
    title: "GNav",
    theme: g.tema,
    routes: {
      '/': (context) => MyApp(),
      '/country': (context) => PageCountry(
            title: g.selectedCountry,
          )
    },
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  Widget _widgetList(BuildContext ctx) {
    switch (_selectedIndex) {
      case 0:
        return Home();
        break;
      case 1:
        return ListSavedCountry(ctx: context);
        break;
      case 2:
        {
          if (g.list == null) {
            g.assigment();
            g.assign();
            setState(() {});
          }
          return ListCountry(
            ctx: context,
          );
        }
        break;
    }
  }

  Future<Widget> _wifi() async {
    try {
      var _result = await InternetAddress.lookup('google.com');
      if (_result.isNotEmpty && _result[0].rawAddress.isNotEmpty) {
        return Icon(Icons.wifi);
      }
    } on SocketException catch (_) {
      return Icon(Icons.signal_wifi_off);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: FutureBuilder(
              future: _wifi(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data;
                } else
                  return Container();
              }),
          title: TextField(
            controller: g.textController,
            onChanged: (x) => setState(() {
              print(g.filtredList);
              g.filtredList = g.list.where((x) {
                bool b = (x[0] ?? '')
                        .toUpperCase()
                        .startsWith(g.textController.text.toUpperCase()) ||
                    (x[1] ?? '')
                        .toUpperCase()
                        .startsWith(g.textController.text.toUpperCase()) ||
                    (x[2] ?? '')
                        .toUpperCase()
                        .startsWith(g.textController.text.toUpperCase());
                return b;
              }).toList();
              _selectedIndex = 2;
            }),
            textAlignVertical: TextAlignVertical.bottom,
            decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintStyle: TextStyle(color: Colors.grey),
                hasFloatingPlaceholder: false,
                hintText: 'Search',
                suffixIcon: Icon(Icons.search, color: Colors.grey)),
            style: TextStyle(color: Colors.white),
          )),
      body: Center(child: _widgetList(context)),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
        ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
            child: GNav(
                gap: 8,
                activeColor: Colors.white,
                iconSize: 24,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 800),
                tabBackgroundColor: Colors.grey[800],
                tabs: [
                  GButton(
                    icon: LineIcons.home,
                    text: 'Home',
                  ),
                  GButton(
                    icon: LineIcons.heart_o,
                    text: 'Likes',
                  ),
                  GButton(
                    icon: LineIcons.search,
                    text: 'Search',
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }
}
