import 'dart:convert';

import 'package:covid/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'api.dart' as api;



class ListCountry extends StatefulWidget {
  BuildContext ctx;
  ListCountry({this.ctx});
  @override
  _ListCountryState createState() => _ListCountryState();
}

class _ListCountryState extends State<ListCountry> {
  @override
  void initState() {
    //assigment();
    super.initState();
  }

 
  @override
  Widget build(BuildContext context) {
    return list == null
        ? Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                    Container(
                        height: 100,
                        width: 100,
                        child: CircularProgressIndicator())
                  ]))
        : ListView.builder(
            itemCount: filtredList.length,
            itemBuilder: (context, index) => ListTile(
                  onTap: () {
                    selectedCountry = filtredList[index][0];
                    Navigator.of(widget.ctx).pushNamed('/country');
                  },
                  title: Text(filtredList[index][0]),
                ));
  }
}
