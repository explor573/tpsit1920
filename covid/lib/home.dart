import 'package:covid/pageCountry.dart';
import 'package:flutter/material.dart';
import 'api.dart';
import 'globals.dart';
import 'api.dart';

class Home extends StatefulWidget {
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  World current;

  @override
  void initState() {
    _getData();
    super.initState();
  }

  void _getData() async {
    current = await world();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: (current != null)
            ? Center(
                child: Column(children: [
                Container(
                    padding: EdgeInsets.all(15),
                    child: Column(children: [
                      Icon(
                        Icons.public,
                        size: 70.0,
                      ),
                      Text(
                        'World',
                        style: TextStyle(fontSize: 30),
                      ),
                    ])),
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '${current.deaths ?? '0'}',
                        ),
                        subtitle: Text(
                          'deaths',
                          style: TextStyle(color: Colors.red),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          '${current.critical ?? '0'}',
                        ),
                        subtitle: Text(
                          'criticals',
                          style: TextStyle(color: Colors.orange),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          '${current.recovered ?? '0'}',
                        ),
                        subtitle: Text(
                          'recovered',
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          '${current.tests ?? '0'}',
                        ),
                        subtitle: Text('tests'),
                      ),
                      ListTile(
                        title: Text(
                          '${current.active ?? '0'}',
                        ),
                        subtitle: Text('actives'),
                      )
                    ],
                  ),
                )
              ]))
            : Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                    Container(
                        height: 100,
                        width: 100,
                        child: CircularProgressIndicator())
                  ])));
  }
}
