import 'dart:convert';
import 'dart:core';

import 'package:http/http.dart' as http;

Future<http.Response> fetchCovid(String nomePaese) {
  try{
  return http.get('https://corona.lmao.ninja/v2/countries/$nomePaese/');
  }catch(error){print(error);}
}

class Country {
  final int id;
  final String country;
  final String flag;
  final int cases;
  final int todayCase;
  final int deaths;
  final int todayDeaths;
  final int recovered;
  final int active;
  final int critical;
  final int tests;

  Country(
      {this.id,
      this.country,
      this.flag,
      this.cases,
      this.todayCase,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.active,
      this.critical,
      this.tests});

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
      id: json['countryInfo']['id'] ,
      country: json['country'] ,
      flag: json['countryInfo']['flag'],
      cases: json['cases'] ,
      todayCase: json['todayCase'] ,
      deaths: json['deaths'],
      todayDeaths: json['todayDeaths'] ,
      recovered: json['recovered'] ,
      active: json['active'] ,
      critical: json['critical'] ,
      tests: json['tests'] ,
    );
  }
}

class World {
  final int cases;
  final int todayCase;
  final int deaths;
  final int todayDeaths;
  final int recovered;
  final int active;
  final int critical;
  final int tests;

  World(
      {this.cases,
      this.todayCase,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.active,
      this.critical,
      this.tests});
  factory World.fromJson(Map<String, dynamic> json) {
    return World(
      cases: json['cases'] ,
      todayCase: json['todayCase'] ,
      deaths: json['deaths'],
      todayDeaths: json['todayDeaths'] ,
      recovered: json['recovered'] ,
      active: json['active'] ,
      critical: json['critical'] ,
      tests: json['tests'] ,
    );
  }
}

Future<http.Response> getAll() {
  try{
  return http.get('https://corona.lmao.ninja/v2/countries');
  }catch(error){print(error);}
}

Future<Country> fetchCountry(String country) async {
  final response = await fetchCovid(country);

  if (response.statusCode == 200) {
    return Country.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to download your country');
  }
}

Future<http.Response> fetchWorld() {
  try{
  return http.get('https://corona.lmao.ninja/v2/all');
  } catch(e){print(e);}
}



Future<World> world() async {
  final response = await fetchWorld();
  if (response.statusCode == 200) {
    return World.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to download all data');
  }
}
