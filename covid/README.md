# Covid

## A cosa serve

App che attraverso la richiesta delle API da [questo sito](https://corona.lmao.ninja/), visualizza le informazioni fornite e le rielabora per poterle renderle presentabili.  

## Come funziona

L'app dispone di tre pagine principali:

- Home: vengono visualizzati i dati relativi all'intero mondo.

- Saved: vengono visualizzati gli stati salvati tra i preferiti, quindi viene fornito un accesso rapido e la possibilità di rimuoverli in caso non se ne necessitino più

- Search: viene visualizzata una lista con tutti i paesi di cui noi abbiamo i dati, e c'è la possibilità di cercare quello che ci interessa

Sopra ogni pagina è presente una barra di ricerca, la quale quando viene premuta porta alla schermata search e vengono mostrati gli stati scremati dalla nostra ricerca.  
Possono essere untilizzate anche le sigle degli stati (esempio: ESP->Spain).  
Vicino alla barra di ricerca quando si è ancora su una delle pagine principali viene mostrata sul lato sinistro della navbar una Icona che dice se è presente o meno il collegamento a Internet. Nel caso non ci sia l'icona cambierà.  
Per salvare gli stati preferiti è stato implementato un database che fa utilizzo dei DAO, e volendo si possono creare altre colonne per registrare i dati più importanti che possono essere aggiunti in futuro.

## Pagina Home

![alt text](./imageReadme/home.png)

## Pagina Favourites Countries

![alt text](./imageReadme/saved.png)

## Pagina Search

![alt text](./imageReadme/search1.png)
![alt text](./imageReadme/search2.png)
![alt text](./imageReadme/search3.png)
