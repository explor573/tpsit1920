# game

Questa applicazione implementa il gioco del 15, utilizzando una animazione per spostare una casella a un'altra.
E' stata implementata una funzione che una volta orginato lo schema controlla che sia effettivamente possibile risolverlo, attraverso un algoritmo spiegato [qui](http://proooof.blogspot.com/2007/12/il-gioco-del-15.html)

Inotre è stato aggiunto un cronometro, la cui implementazione può essere vista [qui](https://gitlab.com/explor573/tpsit1920/-/tree/master/cronometro), che poi era la prima consegna eseguita. 

In fondo alla pagina è piazzato un contatore di mosse che si azzera ogni volta che si comincia il gioco, e sopra di esso è presente un tasto che fa ripartire l'applicazione. Ulteriormente sopra questo tasto c'è uno spazio vuoto che viene riempito con la scritta "HAI VINTO" quando si riesce a risolvere il gioco. 

## Il gioco è sempre risolvibile
