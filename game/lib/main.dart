import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  List<List<int>> field = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 0]
  ];
  List<AnimationController> _controller = [];
  List<Animation<Offset>> _offset = [];
  int _numeroMosse = 0;

  int _counterTime = 0;
  bool _attivo = false;
  String _testoCronometro = "0'0''0";

  Future _cambiaStato() async {
    if (_attivo) {
      Stream<int> stream = _timeCounter();
      stream.listen((onData) {
        setState(() {
          _testoCronometro = _formatoTempo();
          _aggiornaContatore();
        });
      });
    }
  }

  void _aggiornaContatore() {
    _counterTime++;
  }

  void _reset() {
    setState(() {
      _counterTime = 0;
      _testoCronometro = _formatoTempo();
    });
  }

  void start() {
    _attivo = true;
    _reset();
    _cambiaStato();
  }

  void stop() {
    _attivo = false;
    _cambiaStato();
  }

  String _formatoTempo() {
    return '${(_counterTime / 600).floor()}' +
        "'" +
        '${(_counterTime / 10).floor() % 60}' +
        "''" +
        '${(_counterTime % 10).floor()}';
  }

  Stream<int> _timeCounter() async* {
    while (_attivo) {
      await Future.delayed(Duration(milliseconds: 100));
      yield 0;
    }
  }

  @override
  void initState() {
    super.initState();
    _restart();
    for (int i = 0; i < 16; i++) {
      _controller.add(AnimationController(
          vsync: this, duration: Duration(milliseconds: 250)));
      _offset.add(Tween<Offset>(begin: Offset.zero, end: Offset(1.0, 0.0))
          .animate(_controller[i]));
    }
  }

  bool _controllaSchema() {
    int _temp = 0;
    for (int k = 0; k < 4; k++)
      for (int f = 0; f < 4; f++)
        for (int i = k; i < 4; i++)
          for (int j = f; j < 4; j++)
            if (field[k][f] > field[i][j] && field[i][j] != 0) _temp++;

    print(_temp);
    return _temp % 2 == 0;
  }

  _restart() {
    Random rng = Random();
    List<int> _myList;
    int _counter;

    do {
      _myList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
      _counter = 16;
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          field[i][j] = _myList.removeAt(rng.nextInt(_counter--));
        }
      }

      print(_controllaSchema());
    } while (!_controllaSchema());
    print(field.toString());
  }

  void _spostamento(
      int index, int i, int j, double a, double b, int val1, int val2) {
    if (_numeroMosse == 0) start();
    _numeroMosse++;
    _offset[index] = Tween<Offset>(begin: Offset.zero, end: Offset(a, b))
        .animate(_controller[index]);
    _controller[index].forward().whenComplete(() {
      setState(() {
        _controller[index].reset();
        field[i + val1][j + val2] = field[i][j];
        field[i][j] = 0;
      });
    });
    setState(() {});
  }

  _press(int index) {
    int i = index ~/ 4;
    int j = index % 4;
    double d = 1.05;

    //SINISTRA
    if (j != 0 && field[i][j - 1] == 0) {
      _spostamento(index, i, j, -d, 0.0, 0, -1);
    } else
    //DESTRA
    if (j != 3 && field[i][j + 1] == 0) {
      _spostamento(index, i, j, d, 0.0, 0, 1);
    } else
    //SOPRA
    if (i != 0 && field[i - 1][j] == 0) {
      _spostamento(index, i, j, 0.0, -d, -1, 0);
    } else
    //SOTTO
    if (i != 3 && field[i + 1][j] == 0) {
      _spostamento(index, i, j, 0.0, d, 1, 0);
    }
  }

  bool _check() {
    if ((listEquals(field[0], [1, 2, 3, 4]) &&
        listEquals(field[1], [5, 6, 7, 8]) &&
        listEquals(field[2], [9, 10, 11, 12]) &&
        listEquals(field[3], [13, 14, 15, 0]))) {
      stop();
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              _testoCronometro,
              style: TextStyle(fontSize: 50.0),
            ),
            Flexible(
              flex: 9,
              child: GridView.count(
                padding: new EdgeInsets.all(40.0),
                crossAxisCount: 4,
                children: List.generate(16, (idx) {
                  return Container(
                    padding: EdgeInsets.all(5.0),
                    child: SlideTransition(
                      position: _offset[idx],
                      child: Container(
                        decoration: const BoxDecoration(
                          border: Border(
                            top: BorderSide(
                                width: 1.0, color: Color(0xFFFFFFFFFF)),
                            left: BorderSide(
                                width: 1.0, color: Color(0xFFFFFFFFFF)),
                            right: BorderSide(
                                width: 1.0, color: Color(0xFFFF000000)),
                            bottom: BorderSide(
                                width: 1.0, color: Color(0xFFFF000000)),
                          ),
                        ),
                        child: FlatButton(
                          textColor: Colors.black,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: Text(
                            '${field[idx ~/ 4][idx % 4] != 0 ? field[idx ~/ 4][idx % 4] : ''}',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            _press(idx);
                          },
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ),
            Text(
              _check() ? 'HAI VINTO!' : '',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            Container(
              width: 0,
              height: 15,
            ),
            Flexible(
              flex: 2,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    FloatingActionButton(
                      backgroundColor: Colors.white,
                      child: Icon(Icons.autorenew, color: Colors.black),
                      onPressed: () {
                        setState(() {
                          stop();
                          _restart();
                          _numeroMosse = 0;
                        });
                      },
                      heroTag: 'restartGame',
                    ),
                  ]),
            ),
            Container(
              width: 0,
              height: 15,
            ),
            Flexible(
              flex: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Numero mosse: $_numeroMosse'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
