# TPSIT 2019-2020

In questa cartella vengono salvati tutti i progetti di TPSIT eseguiti durante l'anno scolastico 2019/2020, relaizzati in flutter.

I progetti svolti finora sono:

- Cronometro: un widget cronometro
- CRUD: applicazione presa dal professore e modificata per fare un crud completo
- Chatroom: una chatroom implementata coi WebSocket e con chat private
- Echo: Applicazione per esercitarsi nella realizzazione di un server echo con i Socket
- Game: Gioco del 15 implementato per esercitarsi con la grafica
- test_dimensioni: esercizio per usare il BLOC e il drag and drop
- Covid: app che usa le api per mostrare i dari aggiornati sul CoronaVirus
