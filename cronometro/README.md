# cronometro


- [Documentazione flutter](https://api.flutter.dev/index.html)
- [Documentazione per l'implementazione di lacune parti di codice](https://pusher.com/tutorials/flutter-listviews)

Il programma utilizza gli stream per effettuare i suoi calcoli temporali,
per l'implementazione della parte grafica si è fatto uso di una linea in cui
sono posizionati 4 bottoni, uno per ogni funzione implementata, che sono:

- Start e pausa del cronometro
- Reset del cronometro
- Salvataggio tempo parziale
- Reset di tutti i tempi parziali.

Inoltre per contenere tutti i risultati parziali è stata implementata una lista,
ma non si possono selezionare ed eliminare i valori della lista singolarmente.
Inoltre il programma è stato testato su Xiaomi A2 Lite, quindi su dispositivi con risoluzioni
diverse alcune scritte potrebbero essere sfalsate in quanto non ho trovato documentazione che
mi permettesse di rendere adattabile il codice alle diverse risoluzioni.
Infine il tasto reset cancella volutamente il cronometro anche se questo continua ad andare,
continuando a tenere il tempo, per possibili utilizzi futuri.
