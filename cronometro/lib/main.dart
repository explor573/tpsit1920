import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cronometro',
      theme: ThemeData(
        brightness: Brightness.dark,
        accentColor: Colors.green,
        primaryColor: Colors.green,
      ),
      home: MyHomePage(title: 'Cronometro'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counterTime = 0; //tempo trascorso
  bool _attivo = false; // variabile per decretare se il cronometro è attivo o no
  String _testoCronometro = "0'0''0"; // cronometro in formato stringa
  List<String> _tempiParziali = List(); //lista contenente tutti i tempi parziali
  var _immagineBottone = Icon(Icons.play_circle_filled); //immagine che viene cambiata ogni volta che il cronometro cambia stato

  Future _cambiaStato() async { //funzione che si occupa di gestire il cronometro
    _attivo = !_attivo;
    setState(() {
      if (_attivo)
        _immagineBottone = Icon(Icons.pause_circle_filled);
      else
        _immagineBottone = Icon(Icons.play_circle_filled);
    });
    if (_attivo) { // serve per il tempo, grazie a questo funziona il cronometro
      Stream<int> stream = _timeCounter();
      stream.listen((onData) {
        setState(() {
          _testoCronometro = _formatoTempo();
          _aggiornaContatore();
        });
      });
    }
  }

  void _aggiornaContatore() { 
    _counterTime++;
  }

  void _salvaParziale() { //salva il tempo parziale nella lista
    setState(() {
      _tempiParziali.insert(0,
          (_tempiParziali.length + 1).toString() + ':  ' + _formatoTempo());
    });
  }

  void _reset() { // azzera il cronometro
    setState(() {
      _counterTime = 0;
      _testoCronometro = _formatoTempo();
    });
  }

  String _formatoTempo() {// converte l'intero del tempo nel formato da stampare
    return '${(_counterTime / 600).floor()}' +
        "'" +
        '${(_counterTime / 10).floor()% 60}' +
        "''" +
        '${(_counterTime % 10).floor()}';
  }

  Stream<int> _timeCounter() async* {
    while (_attivo) {
      await Future.delayed(Duration(milliseconds: 100));
      yield 0;
    }
  }

  Widget _tastoStartStop() {
    return Align(
        alignment: Alignment.centerLeft,
        child: FloatingActionButton(
          child: _immagineBottone,
          onPressed: () {
            _cambiaStato();
          },
        ));
  }

  Widget _tastoReset() {
    return Align(
        alignment: Alignment.center,
        child: FloatingActionButton(
          child: Icon(Icons.stop),
          backgroundColor: Colors.red,
          onPressed: () {
            _reset();
          },
        ));
  }

  Widget _tastoParziale() {
    return Align(
      alignment: Alignment.centerRight,
      child: FloatingActionButton(
        child: Icon(Icons.add_circle),
        backgroundColor: Colors.yellow,
        onPressed: () {
          _salvaParziale();
        },
        tooltip: 'Aggiungi Parziale',
      ),
    );
  }

  Widget _listaTempiParziali() {
    return ListView.builder(
        itemCount: _tempiParziali.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(_tempiParziali[index],
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 50),
            ),
            
          );
        });
  }

  void _eliminaParziali() {
    setState(() {
      _tempiParziali.clear();
    });
  }

  Widget _tastoEliminaParziali() {
    return Align(
      alignment: Alignment.center,
      child: FloatingActionButton(
        child: Icon(Icons.block),
        backgroundColor: Colors.orange,
        onPressed: () {
          _eliminaParziali();
        },
        tooltip: 'Elimina tempi parziali',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              _testoCronometro,
              style: TextStyle(fontSize: 75.0),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: _tastoStartStop(),
              ),
              Container(
                width: 20,
              ),
              Container(child: _tastoReset()),
              Container(
                width: 20,
              ),
              Container(child: _tastoParziale()),
              Container(
                width: 20,
              ),
              Container(
                child: _tastoEliminaParziali(),
              )
            ],
          ),
          Expanded(
            child: _listaTempiParziali(),
          )
        ],
      ),
    );
  }
}