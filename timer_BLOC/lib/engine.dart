import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import 'ticker.dart';

class TimerBloc extends Bloc<Event, Stato> {
  final Ticker _ticker;
  final int _duration = 10;

  StreamSubscription<int> _tickerSubscription;

  TimerBloc({@required Ticker ticker})
      : assert(ticker != null),
        _ticker = ticker;

  @override
  Stato get initialState => Stato( uState: UserState.ready, seconds:_duration );

  @override
  void onTransition(Transition<Event,Stato> transition) {
    super.onTransition(transition);
    //print(transition);
    //print(state.uState);
  }

  @override
  Stream<Stato> mapEventToState(Event event) async* {
  switch (event.event) {
      case UserEvent.start :
      yield* _mapStartToState(event);
      break;

      case UserEvent.pause :
      yield* _mapPauseToState();
      break;

      case UserEvent.resume :
      yield* _mapResumeToState();
      break;

      case UserEvent.tick :
      yield* _mapTickToState(event);
      break;

      case UserEvent.reset :
      yield* _mapResetToState();
      break;
    default: break;
  }
  }


  @override
  Future<void> close() {
    _tickerSubscription?.cancel();
    return super.close();
  }

  // start
  Stream<Stato> _mapStartToState(Event start) async* {
     yield Stato(uState:UserState.started, seconds: start.duration);
    _tickerSubscription?.cancel();
    _tickerSubscription = _ticker
        .tick(ticks: start.duration)
        .listen((duration) => add(Event(event:UserEvent.tick, duration: duration)));
  }

  // pause
  Stream<Stato> _mapPauseToState() async* {
    if (state.uState == UserState.started) {
      _tickerSubscription?.pause();
      yield Stato(uState:UserState.paused, seconds: state.seconds);
    }
  }

  // resume
  Stream<Stato> _mapResumeToState() async* {
    if (state.uState == UserState.paused) {
      _tickerSubscription?.resume();
      yield Stato(uState:UserState.started, seconds: state.seconds);
    }
  }

  // reset
  Stream<Stato> _mapResetToState() async* {
    _tickerSubscription?.cancel();
    yield Stato(uState:UserState.ready, seconds: _duration);
  }

  // tick
  Stream<Stato> _mapTickToState(Event tick) async* {
    yield tick.duration >= 0 ? Stato(uState:UserState.started, seconds: state.seconds-1) : Stato(uState:UserState.stopped, seconds: state.seconds);
  }
}


// STATSES

enum UserState { started, stopped, paused, ready}

class Stato{
  Stato({@required this.uState, @required this.seconds});
  int seconds;
  UserState uState; 

 // timer seconds
}



// EVENTS

enum UserEvent { start, stop, pause, tick, resume, reset}

class Event{
  int duration;
  UserEvent event; 
  Event({@required this.event, @required this.duration});

  @override
  String toString() => "$event { duration: $duration }";

}
