import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:test_dimensioni/engine.dart';
import 'myPainter.dart';

class MyDraggable extends StatefulWidget{

  MyDraggable({ this.dimensione }){
    color=Colors.yellow;
  }

  @override
  _MyDraggableState createState() => _MyDraggableState();

  double dimensione;
  Color color;

    
}



class _MyDraggableState extends State<MyDraggable> {

    bool active = true;

  @override
  Widget build(BuildContext context) {
    return active ? Draggable<double>(
            data: widget.dimensione,
            maxSimultaneousDrags: 3,
            child: Container(
              width: widget.dimensione,
              height: widget.dimensione,
              color: widget.color,
            ),
            childWhenDragging: Container(
              width: widget.dimensione,
              height: widget.dimensione,
              color: Colors.transparent,
            ),
            feedback: Container(
              width: widget.dimensione,
              height: widget.dimensione,
              color: widget.color,
            ),
            onDragCompleted: (){
              setState(() {
                active = false;
              });
            },

          ):
          Container(
            width: widget.dimensione,
            height: widget.dimensione,
          );
  }
}

