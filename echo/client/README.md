Client echo
==================

Il client è un altro socket che è formato da due pagine, quella di login e quella di invio del messaggio.
Nella pagina di login si inseriscono l'indirizzo IP e la porta su cui andrà trasmesso il messaggio, se sarà presente la connessione allora si entrerà nella seconda pagina, altrimenti verrà visualizzato un messaggio di errore.
La seconda pagina ha una lista che tiene traccia di tutte le risposte ricevute e le visualizza a tutto schermo, attraverso una ListView. E'possibile eliminare una frase selezionata a propria scelta.