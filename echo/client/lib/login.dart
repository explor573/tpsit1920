import 'dart:io';
import 'package:client/main.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'globals.dart' as globals;

class PaginaLogin extends StatefulWidget {
  PaginaLogin({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PaginaLoginState createState() => _PaginaLoginState();
}

class _PaginaLoginState extends State<PaginaLogin> {
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();
  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }

  void _connetti() async {
    if (globals.connesso) {
      globals.socket.destroy();
      globals.connesso = false;
    }
    if (!globals.connesso) {
      globals.socket = await Socket.connect(globals.iPv4, globals.port)
          .catchError((onError) {
        _showSnackBar("errore");
        return;
      });
      if (globals.socket != null) {

        setState(() {
          globals.connesso = true;
        });
      } 
    }
    globals.socket.listen((List<int> event) {
    String temp=(utf8.decode(event));
    globals.risposte.add(temp);
  });
    _cambiaPagina(context);
  }

  void _cambiaPagina(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => (MyHomePage())),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffloidState,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                Flexible(
                  flex: 5,
                  child: TextFormField(
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        labelText: 'Indirizzo IP*',
                      ),
                      keyboardType: TextInputType.number,
                      onSaved: (String value) {
                        globals.iPv4 = value;
                      }),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                Flexible(
                    flex: 5,
                    child: TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 20),
                          labelText: 'Port*',
                        ),
                        onSaved: (String value) {
                          globals.port = int.parse(value);
                        })),
                Spacer(
                  flex: 1,
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            FlatButton(
              child: Icon(
                Icons.done,
                size: 50,
              ),
              onPressed: _connetti,
            ),
          ],
        ),
      ),
    );
  }
}
