import 'package:flutter/material.dart';
import 'dart:convert';
import 'globals.dart' as globals;
import 'login.dart' as login;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: login.PaginaLogin(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();
  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }

  void _invia(String testo) {
    globals.socket.add(utf8.encode(testo));
    setState(() {});
  }

  void _inserisciInvioTesto() {
    TextEditingController _textController = TextEditingController();
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Inserire testo"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                  child: TextField(
                controller: _textController,
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.symmetric(horizontal: 20),
                ),
              )),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: new Text("Invia"),
              onPressed: () {
                Navigator.of(context).pop();

                _invia(_textController.text);
                setState(() {});
              },
            ),
          ],
        );
      },
    );
  }

  Widget _listaRisposte() {
    return ListView.builder(
        itemCount: globals.risposte.length,
        itemBuilder: (context, index) {
          return Card(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    globals.risposte[index],
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                FlatButton(
                  child: Icon(
                    Icons.delete,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    setState(() {
                      globals.risposte.remove(globals.risposte[index]);
                    });
                  },
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffloidState,
      appBar: AppBar(
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            color: Colors.black,
            onPressed: _inserisciInvioTesto,
            icon: Icon(Icons.add),
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: _listaRisposte(),
            )
          ],
        ),
      ),
    );
  }
}
