Server ChatRoom
===============

Questo server per funzionare utilizza un HttpServer e funziona di conseguenza.

I client si connettono ad esso attraverso dei WebSocket che utilizzano dei file JSON per inviare e ricevere i vari messaggi e le varie operzioni che si intendono svolgere.

Le operazioni sono:

* NICKNAME: si assegna il Nickname per il nuovo utente che si è collegato e si invia la lista di tutti gli utenti altri utenti                      precedentemente attivi, oltre che al client appena connesso.
* MESSAGGIO: utilizzato per l'indirizzamento dei messaggi inviati, nel caso il destinatario sia l'utente 'ChatRoom' allora verrà                     inviato il messaggio a tutti gli utent ìi a esclusione del mittente.
* PARTECIPANTI: utilizzzato solo per scopi futuri e debugging.
* BROADCAST:  utilizzzato solo per scopi futuri e debugging.
* ESCI: utilizzzato solo per scopi futuri e debugging.

Una volta che la connessione con il client viene a mancare il server eliminìerà dalle proprie strutture dati le informazioni relative a quel client.
