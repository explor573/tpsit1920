import 'dart:io';
import 'dart:convert';

ServerSocket server;

Map<String, WebSocket> utenti_socket = {};
Map<WebSocket, String> socket_utenti = {};

void main() {
  utenti_socket['ChatRoom'] = null;
  HttpServer.bind(InternetAddress.anyIPv4, 8080).then((HttpServer server) {
    print("Server started");
    server.serverHeader = "an echo server";
    server.listen((HttpRequest request) {
      // Checks whether the request is a valid WebSocket upgrade request
      if (WebSocketTransformer.isUpgradeRequest(request)) {
        WebSocketTransformer.upgrade(request).then(handleWebSocket);
      } else {
        //print("Regular ${request.method} request for: ${request.uri.path}");
        serveRequest(request);
      }
    });
  });
}

void setName(onData, WebSocket client) {
  dynamic temp = json.decode(onData);
  socket_utenti[client] = temp['TESTO'];
  utenti_socket[temp['TESTO']] = client;
  for (WebSocket s in socket_utenti.keys){
    List <dynamic> tmp = utenti_socket.keys.toList();
    tmp.remove(socket_utenti[s]);
    s.add(json
      .encode({'OPERAZIONE': 'LISTA', 'LISTA': tmp}));
  }
   
}

void removeClient(WebSocket client) {
  String temp = socket_utenti[client];
  socket_utenti.remove(client);
  utenti_socket.remove(temp);
  client.close();
  for (WebSocket s in socket_utenti.keys){
    List <dynamic> tmp = utenti_socket.keys.toList();
    tmp.remove(socket_utenti[s]);
    s.add(json
      .encode({'OPERAZIONE': 'LISTA', 'LISTA': tmp}));
  }
   
}

void inviaLista(WebSocket client) {
  client.add(json
      .encode({'OPERAZIONE': 'LISTA', 'LISTA': utenti_socket.keys.toList()}));
}

void indirizzaMessaggio(onData) {
  dynamic temp = json.decode(onData);
  if (temp['DESTINATARIO'] == 'ChatRoom')
    inviaTutti(onData);
  else
    utenti_socket[temp['DESTINATARIO']].add(onData);
}

void serveRequest(HttpRequest request) {
  request.response.statusCode = HttpStatus.forbidden;
  request.response.reasonPhrase = 'No zio tu non passi';
  request.response.close();
}

void inviaTutti(data) {
  dynamic temp = json.decode(data);
  for (WebSocket s in socket_utenti.keys) {
    if (socket_utenti[s] != temp['MITTENTE']) s.add(data);
  }
}

void handleWebSocket(WebSocket client) {
  socket_utenti[client] = '';
  print('Client connected!');
  client.listen((data) {
    Map<String, dynamic> temp = jsonDecode(data);
    print('Client sent: ${temp["OPERAZIONE"]}');
    switch (temp['OPERAZIONE']) {
      case 'NICKNAME':
        setName(data, client);
        break;
      case 'MESSAGGIO':
        indirizzaMessaggio(data);
        break;

      case 'PARTECIPANTI':
        inviaLista(client);
        break;

      case 'BROADCAST':
        inviaTutti(data);
        break;

      case 'ESCI':
        removeClient(client);
        break;
      default:
        break;
    }
  }, onDone: () {
    removeClient(client);
    print('Client disconnected');
  });
}
