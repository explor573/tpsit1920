import 'package:flutter/material.dart';
import 'dart:convert';
import 'globals.dart' as globals;

class SingleChat extends StatefulWidget {
  SingleChat({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _SingleChatState createState() => _SingleChatState();
}

class _SingleChatState extends State<SingleChat> {
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();
  TextEditingController _textController = TextEditingController(text: '');
  void initState() {
    super.initState();
    globals.flusso.stream.listen((onData) {
      setState(() {});
    });
  }



  void _invia() {
    if (_textController.text.trim() == '') return;
    dynamic temp = {
      'OPERAZIONE': 'MESSAGGIO',
      'MITTENTE': globals.nickname,
      'DESTINATARIO': widget.title,
      'TESTO': _textController.text,
    };
    globals.socket.add(json.encode(temp));
    if (globals.allMessaggi[temp['DESTINATARIO']] == null)
      globals.allMessaggi[temp['DESTINATARIO']] = [];
    globals.allMessaggi[temp['DESTINATARIO']].add(temp);
    print(globals.allMessaggi[temp['DESTINATARIO']]);
    _textController.clear();
    setState(() {});
  }

  Widget _cardDX(String testo) {
    return Row(
      children: <Widget>[
        Spacer(
          flex: 1,
        ),
        Flexible(
         flex: 4,
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(18))),
            child: ListTile(
              title: Text(
                testo,
              ),
              contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
            ),
            color: Colors.grey[200],
            elevation: 0,
          ),
        )
      ],
    );
  }

  Widget _cardSX(String testo) {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 4,
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(18))),
            child: ListTile(
              title: Text(
                testo,
              ),
              contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
            ),
            color: Colors.grey[200],
            elevation: 0,
          ),
        ),
        Spacer(
          flex: 1,
        ),
      ],
    );
  }

  Widget _creaLista() {
    return ListView.builder(
        reverse: false,
        scrollDirection: Axis.vertical,
        itemCount: (globals.allMessaggi[widget.title] == null)
            ? 0
            : globals.allMessaggi[widget.title].length,
        itemBuilder: (context, index) {
          dynamic temp = (globals.allMessaggi[widget.title])[index];
          return (temp['MITTENTE'] == globals.nickname)
              ? _cardDX(temp['TESTO'])
              : _cardSX(temp['TESTO']);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffloidState,
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Colors.black,
          actions: <Widget>[],
        ),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              Expanded(
                child: _creaLista(),
              ),
              Row(
                children: <Widget>[
                  Flexible(
                      flex: 5,
                      child: TextField(
                        controller: _textController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Scrivi il messaggio',
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 5)),
                      )),
                  Flexible(
                    flex: 1,
                    child: FlatButton(
                      onPressed: _invia,
                      child: Icon(
                        Icons.send,
                        color: Colors.black,
                      ),
                    ),
                  )
                ],
              )
            ])));
  }
}
