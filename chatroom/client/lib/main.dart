import 'package:client/chatroom.dart';
import 'package:client/single_chat.dart';
import 'package:flutter/material.dart';
import 'globals.dart' as globals;
import 'login.dart' as login;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'User',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: login.PaginaLogin(title: 'Pagina Login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    globals.flusso.stream.listen((onData) {
      setState(() {});
    });
  }

  void _passaAllaChat(String nomeUtente) {
    setState(() {});
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => (nomeUtente != 'ChatRoom')
                ? SingleChat(
                    title: '$nomeUtente',
                  )
                : ChatRoom(
                    title: '$nomeUtente',
                  )));
  }

  Widget _listaUtenti() {
    setState(() {});
    return ListView.builder(
        reverse: false,
        scrollDirection: Axis.vertical,
        itemCount: globals.utenti.length,
        itemBuilder: (context, index) {
          return Card(
              child: ListTile(
            leading: Icon(
              Icons.supervised_user_circle,
              color: Colors.black,
            ),
            onTap: () => _passaAllaChat(globals.utenti[index]),
            title: Text(globals.utenti[index]),
          ));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffloidState,
      appBar: AppBar(
        title: Text('Utenti attivi'),
        backgroundColor: Colors.black,
        actions: <Widget>[],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: _listaUtenti(),
            )
          ],
        ),
      ),
    );
  }
}
