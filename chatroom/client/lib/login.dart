import 'dart:io';
import 'package:client/main.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'globals.dart' as globals;

class PaginaLogin extends StatefulWidget {
  PaginaLogin({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PaginaLoginState createState() => _PaginaLoginState();
}

class _PaginaLoginState extends State<PaginaLogin> {
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();
  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }

  void _connetti() async {
    globals.socket =
        await WebSocket.connect('ws://${globals.iPv4}:${globals.port}//ws')
            .catchError((onError) {
      _showSnackBar('errore di connessione');
      return;
    });
    if (globals.socket != null) {
      _cambiaPagina(context);
      globals.socket.listen((onData) {
        dynamic temp = json.decode(onData);
        print(temp);
        switch (temp['OPERAZIONE']) {
          case 'LISTA':
            globals.utenti = List<String>.from(temp['LISTA']);
            globals.utenti.remove(globals.nickname);

            break;

          case 'MESSAGGIO':
         
            { 
              if (temp['DESTINATARIO'] == 'ChatRoom') {
               
                if (globals.allMessaggi[temp['DESTINATARIO']] == null)
                  globals.allMessaggi[temp['DESTINATARIO']] = [];
                globals.allMessaggi[temp['DESTINATARIO']].add(temp);
              } else {
                
                if (globals.allMessaggi[temp['MITTENTE']] == null)
                  globals.allMessaggi[temp['MITTENTE']] = [];
                globals.allMessaggi[temp['MITTENTE']].add(temp);
                // print(globals.allMessaggi[temp['DESTINATARIO']]);
              }
            }

            break;
          default:
            break;
        }
        globals.flusso.sink.add(onData);
      });
      globals.socket.add((json.encode({
        'OPERAZIONE': 'NICKNAME',
        'TESTO': globals.nickname,
      })));
    }
  }

  void _cambiaPagina(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => (MyHomePage())),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffloidState,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                Flexible(
                  flex: 5,
                  child: TextFormField(
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        labelText: 'Indirizzo IP*',
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (String value) {
                        globals.iPv4 = value;
                      }),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                Flexible(
                    flex: 5,
                    child: TextFormField(
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 20),
                          labelText: 'Nickname*',
                        ),
                        onChanged: (String value) {
                          globals.nickname = value;
                        })),
                Spacer(
                  flex: 1,
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            FlatButton(
              child: Icon(
                Icons.done,
                size: 50,
              ),
              onPressed: _connetti,
            ),
          ],
        ),
      ),
    );
  }
}
