Client ChatRoom
==================

Il client è un WebSocket che è formato da tre pagine, quella di login, quella contenente tutti gli utenti attivi e quella della chat singola.

Nella pagina di login si inseriscono l'indirizzo IP della macchina contenente il server e il proprio username con cui si verrà identificati. Se sarà presente la connessione allora si entrerà nella pagina contenente tutti gli utenti attivi, altrimenti verrà visualizzato un messaggio di errore.

Nella lista di tutti gli utenti attivi abbiamo oltre agli username degli altri utenti collegati abbiamo una chat nominata ChatRoom: all'interno di quella chat è possibile mandare e ricevere a tutti gli utenti connessi un messaggio e ricevere i loro.
