

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'task.dart';
import 'task_dao.dart';

class ModificaTesto extends StatelessWidget {
  
  Task task;
  TaskDao dao;
  ModificaTesto(this.task, this.dao);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Modifica Testo"),
      ),
      body: TextFormField(
        initialValue: task.message,
        onChanged: (t)async{
          task.message = t;
          await dao.updateTask(task);
          
        },
      ),
    );
  }
}